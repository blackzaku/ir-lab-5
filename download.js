const cheerio = require('cheerio');
const request = require('request');
const URL = require('url');

const COUNT_BY_MEDIA = 10000;
const MEDIA = [
    ["The Washington Post", "166423"],
    ["The Wall Street Journal", "2282"],
    ["The New York Times", "4263"],
    ["The Economist", "166368"],
    ["Forbes", "5597"],
    ["Bloomberg", "6918"],
    ["Financial Times", "4697"],
    ["Business Insider", "683279"],
    ["Mashable", "200000"],
    ["TechCrunch", "85562"],
    ["Wired", "6958"],
];

function getPosts(mediaId, skip = 0, count = 100) {
    const url = `https://www.linkedin.com/biz/${mediaId}/feed?start=${skip}&v2=true&count=${count}`;

    return new Promise((resolve, reject) =>
        request.get(url, (error, response, body) =>
            error ? reject(error) : resolve(cheerio.load(body))))
        .then($ => {
            const posts = [];
            $('li.feed-item').each(function () {
                const linkedInUrl = 'https://www.linkedin.com' + $(this).attr('data-li-single-update-url');
                const date = new Date($(this).attr('data-li-update-date'));
                const description = $(this).find('.commentary').text();
                const title = $(this).find('.title').text();
                const redirectUrl = $(this).find('.title').attr('href');
                const parsedUrl = URL.parse(redirectUrl);
                const url = parsedUrl.searchParams.get('url');
                posts.push({ linkedInUrl, url, date, title, description });
            });
            return posts;
        });

}